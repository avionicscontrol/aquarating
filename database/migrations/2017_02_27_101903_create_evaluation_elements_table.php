<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateEvaluationElementsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'evaluation_elements', function (Blueprint $table) {
			$table->increments ( 'id' );
			$table->timestamps ();
			$table->integer ( 'indicator_n' );
			$table->float ( 'weight' );
			$table->boolean ( 'is_drinking_water' );
			$table->boolean ( 'is_wastewater' );
			$table->enum ( 'type', [ 
					'I',
					'L' 
			] );
			$table->string ( 'condition' );
			// FK
			$table->foreign ( 'formula_id' )->references ( 'id' )->on ( 'formulas' )->onDelete ( 'restrict' );
			$table->foreign ( 'formula_2_id' )->references ( 'id' )->on ( 'formulas' )->onDelete ( 'restrict' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists ( 'evaluation_elements' );
	}
}
