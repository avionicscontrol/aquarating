<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'practice_scope', function (Blueprint $table) {
		$table->integer ( 'practice_id' )->unsigned ()->index ();
		$table->foreign ( 'practice_id' )->references ( 'id' )->on ( 'practices' )->onDelete ( 'cascade' );
		$table->integer ( 'scope_id' )->unsigned ()->index ();
		$table->foreign ( 'scope_id' )->references ( 'id' )->on ( 'scopes' )->onDelete ( 'cascade' );
		$table->primary ( [ 
				'practice_id',
				'scope_id' 
		] );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('practice_scope');
    }
}
