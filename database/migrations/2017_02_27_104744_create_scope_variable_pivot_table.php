<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'scope_variable', function (Blueprint $table) {
		$table->integer ( 'scope_id' )->unsigned ()->index ();
		$table->foreign ( 'scope_id' )->references ( 'id' )->on ( 'scopes' )->onDelete ( 'restrict' );
		$table->integer ( 'variable_id' )->unsigned ()->index ();
		$table->foreign ( 'variable_id' )->references ( 'id' )->on ( 'variables' )->onDelete ( 'restrict' );
		$table->primary ( [ 
				'scope_id',
				'variable_id' 
		] );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scope_variable');
    }
}
