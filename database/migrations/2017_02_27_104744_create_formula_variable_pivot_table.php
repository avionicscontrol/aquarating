<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'formula_variable', function (Blueprint $table) {
		$table->integer ( 'formula_id' )->unsigned ()->index ();
		$table->foreign ( 'formula_id' )->references ( 'id' )->on ( 'formulas' )->onDelete ( 'distinct' );
		$table->integer ( 'variable_id' )->unsigned ()->index ();
		$table->foreign ( 'variable_id' )->references ( 'id' )->on ( 'variables' )->onDelete ( 'distinct' );
		$table->primary ( [ 
				'formula_id',
				'variable_id' 
		] );
		// campo excedente
		$table->string ( 'field' );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('formula_variable');
    }
}
