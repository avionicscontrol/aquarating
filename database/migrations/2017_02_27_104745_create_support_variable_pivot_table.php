<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'support_variable', function (Blueprint $table) {
		$table->integer ( 'support_id' )->unsigned ()->index ();
		$table->foreign ( 'support_id' )->references ( 'id' )->on ( 'supports' )->onDelete ( 'restrict' );
		$table->integer ( 'variable_id' )->unsigned ()->index ();
		$table->foreign ( 'variable_id' )->references ( 'id' )->on ( 'variables' )->onDelete ( 'restrict' );
		$table->primary ( [ 
				'support_id',
				'variable_id' 
		] );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('support_variable');
    }
}
