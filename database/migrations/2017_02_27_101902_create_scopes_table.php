<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateScopesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'scopes', function (Blueprint $table) {
			$table->increments ( 'id' );
			$table->boolean ( 'p' );
			$table->boolean ( 'd' );
			$table->boolean ( 'r' );
			$table->boolean ( 'tr' );
			$table->boolean ( 'fr' );
			$table->boolean ( 'fa' );
			// $table->timestamps();
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists ( 'scopes' );
	}
}
