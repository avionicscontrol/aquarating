<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateNormalizationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'normalizations', function (Blueprint $table) {
			$table->increments ( 'id' );
			$table->timestamps ();
			$table->float ( 'a' );
			$table->float ( 'b' );
			$table->float ( 'c' );
			$table->float ( 'd' );
			$table->float ( 'x_min' );
			$table->float ( 'x_max' );
			$table->integer ( 'before_x' );
			$table->integer ( 'after_x' );
		} );
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists ( 'normalizations' );
	}
}
