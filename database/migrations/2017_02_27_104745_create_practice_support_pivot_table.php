<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'practice_support', function (Blueprint $table) {
		$table->integer ( 'practice_id' )->unsigned ()->index ();
		$table->foreign ( 'practice_id' )->references ( 'id' )->on ( 'practices' )->onDelete ( 'restrict' );
		$table->integer ( 'support_id' )->unsigned ()->index ();
		$table->foreign ( 'support_id' )->references ( 'id' )->on ( 'supports' )->onDelete ( 'restrict' );
		$table->primary ( [ 
				'practice_id',
				'support_id' 
		] );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('practice_support');
    }
}
