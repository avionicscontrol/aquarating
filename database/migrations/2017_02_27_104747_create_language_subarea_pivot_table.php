<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class App\ extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::create ( 'language_subarea', function (Blueprint $table) {
		$table->integer ( 'language_id' )->unsigned ()->index ();
		$table->foreign ( 'language_id' )->references ( 'id' )->on ( 'languages' )->onDelete ( 'restrict' );
		$table->integer ( 'subarea_id' )->unsigned ()->index ();
		$table->foreign ( 'subarea_id' )->references ( 'id' )->on ( 'subareas' )->onDelete ( 'restrict' );
		$table->primary ( [ 
				'language_id',
				'subarea_id' 
		] );
	} );
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('language_subarea');
    }
}
