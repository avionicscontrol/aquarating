@include('includes.headLogin')  
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row ">
        <div class="col-md-8 col-md-offset-2 ">
         <div class="logo">
                    	<img src="http://demo.aquarating.com/public/img/AquaRating_light.png">
         </div>           
            <div class="panel panel-default ">
                <div class="panel-body loginI">
                    <form class="form-horizontal " role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerda
                                    </label>
                                </div>
                            </div>
                        </div>
						<!-- underForm    -->
                        <div class="form-group row">
                         <div class="col-md-4"></div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">
                                    Entrar
                                </button>
							</div>
							<div class="col-md-3">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                  &iquest; olvidastes la clave?
                                </a>  
                             </div> 
                             <div class="col-md-3">                         
                                <div class="dropdown">
    								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Lenguaje
 								   <span class="caret"></span></button>
									    <ul class="dropdown-menu">
									      <li><a href="#">Ingles</a></li>
									      <li><a href="#">Espa&ntilde;a</a></li>
									      <li><a href="#">Frances</a></li>
									    </ul>
  									</div>
  								</div>	
                            </div>
                        </div>
                        <!-- underForm    -->
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')  
@endsection
