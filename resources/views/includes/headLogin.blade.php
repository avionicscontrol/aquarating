<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Styles -->
   <link href="{{ asset('css/login.css') }}" rel="stylesheet">
   <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
   <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap-theme.css.map') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap-theme.min.css.map') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap.css.map') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
   <link href="{{ asset('css/bootstrap.min.css.map') }}" rel="stylesheet">
   <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
   <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- js -->
  	<script src="{{ asset('js/app.js') }}"></script>
  	<script src="{{ asset('js/bootstrap.js') }}" ></script>
  	<script src="{{ asset('js/bootstrap.min.js') }}" ></script>
  	<script src="{{ asset('js/npm.js') }}" ></script>
  <!-- Scripts -->
   <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
   </script> 
</head>
